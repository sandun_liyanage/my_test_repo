<html>
	<head>
    	<script type="text/javascript" src="jquery-1.9.1.js"></script>
        <script type="text/javascript">
        	function split_left_to_right(val, id){
				if(val.length < 5){
					alert("Value is too short...");
				}
				else{
					$('#r_' + id).val(val.substring(3, 5));
				}
			}
        </script>
        <style type="text/css">
        	.container{
				padding-top: 10px;
				margin: 0 auto;
				text-align: center;
				font-family:Verdana, Geneva, sans-serif;
			}
        </style>
    </head>
    <body>
    	<div class="container">
        	<h3>Tets form</h3>
    	<?php
        	for($i=0; $i<=5; $i++){
				?>
                <div>
                    Left value <?php echo $i; ?>
                    <input type="text" name="left_<?php echo $i; ?>" onBlur="split_left_to_right(this.value, '<?php echo $i; ?>');" />
                    Right value <?php echo $i; ?>
                    <input type="text" name="right_<?php echo $i; ?>" id="r_<?php echo $i; ?>" />
                </div>
                <?php
			}
		?>
        </div>
    </body>
</html>